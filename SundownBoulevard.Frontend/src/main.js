import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import WidgetFrame from "./components/WidgetFrame.vue";
import VModal from "vue-js-modal";
var SocialSharing = require("vue-social-sharing");

// Require dependencies
var VueCookie = require("vue-cookie");

// detect what device we are on
// Tell Vue to use the plugin
Vue.use(VueCookie);
Vue.use(VueRouter);
Vue.use(VModal, {
    dialog: true,
    dynamic: true
});

window.axios = require("axios");
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
Vue.prototype.$http = window.axios;


// MENU ROUTES
const routes = [
    {
        path: "/",
        name: "home",
        component: WidgetFrame,
        props: true,
        meta: {
            redirect: '/',
        },
      beforeEnter: (to, from, next) => {

      }
    }

];
// history removes the #
const router = new VueRouter({
    routes
});

//Global API URL - defined in package.json
Vue.prototype.$localUrl = VUE_APP_APIURL;

Vue.filter("capitalize", function(value) {
    if (!value) return "";
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

// Format the date
import moment from "moment";
Vue.filter("formatDate", function(value) {
    if (value) {
      return moment(String(value)).format("DD.MM.YYYY");
    }
});

Vue.filter("formatDateTime", function (value) {
  if (value) {
    return moment(String(value)).format("DD.MM.YYYY, H:MM");
  }
});
Vue.filter("formatTime", function (value) {
  if (value) {
    return moment(String(value)).format("H:MM");
  }
});

import optionsMixin from "./mixins/mixins.js";


new Vue({
    el: "#app",
    router: router,
    render: h => h(App),
    components: { App, WidgetFrame },
    mixins: [optionsMixin],
    data() {
        return {
    };
  }, mounted() {
      window.addEventListener('orientationchange', this.screenChange);
  },created() {
      this.screenChange();
  },
  methods: {
  }
});
