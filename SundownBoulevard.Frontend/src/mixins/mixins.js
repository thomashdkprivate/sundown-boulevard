import VueGridLayout from "vue-grid-layout";
import Vue from "vue";

export default {
    methods: {
        toogleOptions: function () {
            this.hideOptions = !this.hideOptions;
        },
        changeTab: function(n, id, name) {
            this.gtm("tab", "Tab Changed " + n, id, name)
            this.tab = n;
            //TODO change to axios
            var data = JSON.stringify({
                token: this.$route.params.accessToken,
                id: id,
                open: null,
                tabNumber: n
            });
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;

            xhr.addEventListener("readystatechange", function() {
                if (this.readyState === 4) {
                    // Success
                    if (xhr.status === 200) {
                        // console.log("Worked: ", this.responseText);
                    } else {
                        console.log("Error: ", this.responseText);
                    }
                }
            });

            xhr.open(
                "POST",
                this.$localUrl +
                    "/umbraco/api/UserWidget/WidgetIsOpenOrSelected"
            );
            xhr.setRequestHeader("content-type", "application/json");
            xhr.setRequestHeader("cache-control", "no-cache");
            setTimeout(function() {
                xhr.send(data);
            }, 1000);
            this.resize();
        },
        joinArray: function (arr, join) {
            return arr.join(join);
        },
        resize: function () {
            this.$nextTick(function() {
                this.$parent.autoSize();
            });
        },
        resizeFold: function (id) {
            this.open = !this.open;
            var isOpen = this.open ? "1" : "0";
            this.$nextTick(function() {
                this.$parent.autoSize();
            });

            //TODO change to axios
            var data = JSON.stringify({
                token: this.$route.params.accessToken,
                id: id,
                open: isOpen
            });
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            xhr.addEventListener("readystatechange", function() {
                if (this.readyState === 4) {
                    // Success
                    if (xhr.status === 200) {
                        // console.log("Worked: ", this.responseText);
                    } else {
                        console.log("Error: ", this.responseText);
                    }
                }
            });

            xhr.open(
                "POST",
                this.$localUrl +
                    "/umbraco/api/UserWidget/WidgetIsOpenOrSelected"
            );
            xhr.setRequestHeader("content-type", "application/json");
            xhr.setRequestHeader("cache-control", "no-cache");
            setTimeout(function() {
                xhr.send(data);
            }, 1000);
        },
        triggerRemoveWidget: function(id, name) {
            this.$modal.show("dialog-modal", {
                title: this.data.widgetTitle,
                text: name,
                id: id
            });
            this.$nextTick(function() {
                this.$parent.autoSize();
            });
        },
        showInfoModal: function() {
            this.gtm("infobox", "Infobox", this.data.WidgetId,  this.data.widgetTitle)
            this.$modal.show("info-modal", {
                title: this.data.widgetTitle,
                text: this.data.information
            });
        },
        parseXml: function(xml) {
            var dom = null;
            if (window.DOMParser) {
                try {
                    dom = new DOMParser().parseFromString(xml, "text/xml");
                } catch (e) {
                    dom = null;
                }
            } else if (window.ActiveXObject) {
                try {
                    dom = new ActiveXObject("Microsoft.XMLDOM");
                    dom.async = false;
                    if (!dom.loadXML(xml))
                        // parse error ..

                        window.alert(
                            dom.parseError.reason + dom.parseError.srcText
                        );
                } catch (e) {
                    dom = null;
                }
            } else alert("cannot parse xml string!");
            return dom;
        },
        showHistory: function() {
            this.history = !this.history;
            this.$nextTick(function() {
                this.resize();
            });
        },
        saveToHistory: function(link) {
            var vm = this;
            var id = vm.uniqueID();
            link.linkId = id;

            if (this.historyList !== undefined) {
                this.history = false;
                this.historyList.unshift(link);
                vm.$forceUpdate();
            }

            var linkArr = [];
            linkArr.push(link);
            var url = this.$localUrl + "/umbraco/api/Shortcut/Add";
            this.$http
                .post(url, linkArr, {
                    headers: {
                        "Content-type": "application/json"
                    }
                })
                .then(function(response) {
                    console.log("History response: ", response.data);
                })
                .catch(function(error) {
                    console.log(error);
                });
            return id;
        },
        updateHistory: function(link) {
            var linkArr = [];
            linkArr.push(link);
            var url = this.$localUrl + "/umbraco/api/Shortcut/Update";
            this.$http
                .post(url, linkArr, {
                    headers: {
                        "Content-type": "application/json"
                    }
                })
                .then(function(response) {
                    console.log("History response: ", response.data);
                })
                .catch(function(error) {
                    console.log(error);
                });
            return "Link updated";
        },
        uniqueID: function () {
            var crypto = window.crypto || window.msCrypto;
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (
                    c ^
                    (crypto.getRandomValues(new Uint8Array(1))[0] &
                        (15 >> (c / 4)))
                ).toString(16)
            );
        },
        removeZindex: function (){
            // Remove all class on-top
            var els = document.querySelectorAll('.vue-grid-item.on-top')
            _removeClasses()
            function _removeClasses() {
                for (var i = 0; i < els.length; i++) {
                els[i].classList.remove('on-top')
                }
            }
        },
        addZindex: function(e){
            this.removeZindex();
            e.target.closest('.vue-grid-item').classList.add('on-top');
        },
        getDicItem: function (key) {

          var item = JSON.parse(localStorage.getItem('dic'));
          if (item != null) {
            return item[key];
          } else {
            setTimeout(this.getDicItem, 250);
          }

      },
        capitalize: function (value) {
          if (!value) return "";
          value = value.toString();
          return value.charAt(0).toUpperCase() + value.slice(1);
        },
      listLimited: function (list, tab) {
          var sortedlist = list.filter((x, i) => { return x.tabNumber == tab });
          return sortedlist.slice(0, 5);
      },
      gtm: function (event, type, id, name, cat = "widget"){
        var label = id && name == "null" ? type : type + " Widget: " + id + " - " + name;
        this.$gtm.trackEvent({
          event: event, // Event type [default = 'interaction'] (Optional)
          category: cat,
          action: 'click',
          label: label,
          value: this.data,
          noninteraction: false // Optional
        });
      }
    }
};
