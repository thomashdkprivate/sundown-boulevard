# Sundown.vue

> Sundown

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080 - remember the access token (ex. http://localhost:8080/#/584c2b7b75bfdda4a5f5f4dd98568c29 )
npm run dev

# build for production with minification
npm run build
```
