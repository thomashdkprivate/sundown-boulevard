var path = require("path");
var webpack = require("webpack");
var Uglify = require("uglifyjs-webpack-plugin");
var ImageminPlugin = require('imagemin-webpack-plugin').default;


module.exports = {
    entry: ["./src/main.js"],
    output: {
        path: path.resolve("../SundownBoulevard/assets/"),
        publicPath: "/dist/",
        filename: "vue.js"
    },
    module: {
      rules: [
            {
                test: /\.css$/,
                use: ["vue-style-loader", "css-loader"]
            },
            {
                test: /\.scss$/,
                use: ["vue-style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.sass$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    "sass-loader?indentedSyntax"
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                        // the "scss" and "sass" values for the lang attribute to the right configs here.
                        // other preprocessors should work out of the box, no loader config like this necessary.
                        scss: [
                            "vue-style-loader",
                            "css-loader",
                            {
                                loader: "sass-loader",
                                options: {
                                    data: '@import "~styles/_app.scss";'
                                }
                            }
                        ],
                        sass: [
                            "vue-style-loader",
                            "css-loader",
                            "sass-loader?indentedSyntax"
                        ]
                    }
                    // other vue-loader options go here
                }
            },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          }
        },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: "file-loader",
                options: {
                    name: "[name].[ext]?[hash]"
                }
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: "url-loader?limit=100000"
            }
        ]
    },
    resolve: {
        alias: {
            vue$: "vue/dist/vue.esm.js",
            // Defines the root to the style folder
            styles: path.resolve(__dirname, "./src/scss"),
            js: path.resolve(__dirname, "./src/js"),
            fonts: path.resolve(__dirname, "./src/fonts"),
            json: path.resolve(__dirname, "./src/json")
        },
        extensions: ["*", ".js", ".vue", ".json"]
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true,
        overlay: true
    },
    performance: {
        hints: false
    },
    plugins: [
        // Global api url
        new webpack.DefinePlugin({
        VUE_APP_APIURL: "''",
        'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new Uglify({
            sourceMap: true,
            uglifyOptions: {
                output: {
                    beautify: false
                }
            }
        }),
        new ImageminPlugin({
            disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '80'
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]
    // devtool: '#eval-source-map
};

