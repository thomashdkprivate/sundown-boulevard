var path = require("path");
var webpack = require("webpack");

module.exports = {
    devtool: "source-map",
    entry: [
        'webpack-dev-server/client?http://127.0.0.1',
        path.resolve(__dirname, "./src/main.js")],
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "/dist/",
        filename: "app.js"
    },
    module: {
        rules: [
            {
                test: /\.css$/,
            use: ["vue-style-loader",
              { loader: 'css-loader', options: { importLoaders: 1 } },
              'postcss-loader']
            },
            {
                test: /\.sass$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    "sass-loader?indentedSyntax"
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                        // the "scss" and "sass" values for the lang attribute to the right configs here.
                        // other preprocessors should work out of the box, no loader config like this necessary.
                        scss: [
                            "vue-style-loader",
                            "css-loader",
                            {
                                loader: "sass-loader",
                                options: {
                                  data: '@import "~styles/_app.scss";',
                                  sourceMap: true
                                }
                            }
                        ],
                        sass: [
                            "vue-style-loader",
                            "css-loader",
                            "sass-loader?indentedSyntax"
                        ]
                    }
                    // other vue-loader options go here
                }
            },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
            }
          },
            {
                test: /\.(png|jpg|gif|svg|ico)$/,
                loader: "file-loader",
                options: {
                    name: "[name].[ext]?[hash]"
                }
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: "url-loader?limit=100000"
            }
        ]
    },
    resolve: {
        alias: {
            vue$: "vue/dist/vue.esm.js",
            // Defines the root to the style folder
            styles: path.resolve(__dirname, "./src/scss"),
            fonts: path.resolve(__dirname, "./src/fonts"),
            json: path.resolve(__dirname, "./src/json"),
            image: path.resolve(__dirname, "./src/images")
        },
        extensions: ["*", ".js", ".vue", ".json"]
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true
    },
    performance: {
        hints: false
    },
    plugins: [
        // GLobal apiurl
        new webpack.DefinePlugin({
            VUE_APP_APIURL: "'http://sunset.local/'"
        })

    ]
    // devtool: '#eval-source-map'
};
