using SundownBoulevard.Core.Helpers;
using SundownBoulevard.Core.Models;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.WebApi;

namespace SundownBoulevard.Core.Service
{
    public class SunDownHelperServive : ICabanaHelperService
    {
        protected new ServiceContext Services { get; }
        private readonly IMemberService _memberService;
        public SunDownHelperServive(ServiceContext services, IMemberService memberService)
        {
            Services = services;
            _memberService = memberService;

        }
        /// <summary>
        /// Get Member or create on if new member
        /// </summary>
        /// <param name="Email"></param>
        /// <returns>A Existing or new Member</returns>
        public StatusViewModel GetMember(string Email, int confirmPageId)
        {
            StatusViewModel response = new StatusViewModel();
            response.member = Services.MemberService.GetByUsername(Email);
            // Check if the member exists in the system
            if (response.member == null)
            {
                // else create a member
                response.member = Services.MemberService.CreateMember(Email, Email, Email, "pageSubscribes");
                response.member.IsApproved = false;
                response.isNew = true;
                response.isApproved = false;
                Services.MemberService.Save(response.member);
                SendConfirmMail(Email, confirmPageId);
            }
            else if (!response.member.IsApproved)
            {
                response.isNew = false;
                response.isApproved = false;
                SendConfirmMail(Email, confirmPageId);
            }
            else
            {
                response.isApproved = true;
            }
            return response;
        }
        /// <summary>
        /// Get's the member
        /// </summary>
        /// <param name="Email"></param>
        /// <returns>Returns a member if exisit</returns>
        public IMember VerifyMember(string Email)
        {
            IMember member = null;
            if (!string.IsNullOrEmpty(Email))
            {
                member = Services.MemberService.GetByUsername(Email);
            }
            return member;
        }

        public bool SendConfirmMail(string Email, int confirmPageId)
        {
            var confirmPageUrl = Current.UmbracoContext.UrlProvider.GetUrl(confirmPageId);
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            string host = section.Network.Host;
            string user = section.Network.UserName;
            string password = section.Network.Password;
            int port = section.Network.Port;

            SmtpClient client = new SmtpClient
            {
                Port = port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = host,
                Credentials = new System.Net.NetworkCredential(user, password)
            };
            MailMessage NotificationMail = new MailMessage("no-reply@cabanabaseline.dk", Email);
            NotificationMail.IsBodyHtml = true;
            NotificationMail.Subject = "Bekræft venlist";

            string path = HttpContext.Current.Server.MapPath("~/EmailTemplates/SubscribePageConfirmMail.html");
            string rootUrl = HttpContext.Current.Request.Url.Host;
            string content = System.IO.File.ReadAllText(path);

            var encrypted = Urlencrypto.GetEncryptedQueryString(Email);
            var link = rootUrl + confirmPageUrl + "?q=" + encrypted;
            content = content.Replace("[confirmUrl]", link);
            NotificationMail.Body = content;

            client.Send(NotificationMail);
            return true;
        }
        public bool SendLoginMail(string Email, int memberPageId)
        {
            var memberPageUrl = Current.UmbracoContext.UrlProvider.GetUrl(memberPageId);
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            string host = section.Network.Host;
            string user = section.Network.UserName;
            string password = section.Network.Password;
            int port = section.Network.Port;

            SmtpClient client = new SmtpClient
            {
                Port = port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = host,
                Credentials = new System.Net.NetworkCredential(user, password)
            };
            MailMessage NotificationMail = new MailMessage("no-reply@cabanabaseline.dk", Email);
            NotificationMail.IsBodyHtml = true;
            NotificationMail.Subject = "Login Email";

            string path = HttpContext.Current.Server.MapPath("~/EmailTemplates/SubscribePageConfirmMail.html");
            string rootUrl = HttpContext.Current.Request.Url.Host;
            string content = System.IO.File.ReadAllText(path);

            var encrypted = Urlencrypto.GetEncryptedQueryString(Email);
            var link = rootUrl + memberPageUrl + "?q=" + encrypted;
            content = content.Replace("[confirmUrl]", link);
            NotificationMail.Body = content;

            client.Send(NotificationMail);
            return true;
        }
    }
    public interface ICabanaHelperService
    {
        StatusViewModel GetMember(string email, int confirmPageId);
        IMember VerifyMember(string email);
        bool SendLoginMail(string email, int memberPageId);
    }
}